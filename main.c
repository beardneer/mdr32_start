//
 #include <MDR32F9Qx_port.h>
 #include <MDR32F9Qx_rst_clk.h>
 #include <MDR32F9Qx_power.h>
 #include <MDR32F9Qx_ssp.h>
 
/* Private typedef -----------------------------------------------------------*/


/* Private define ------------------------------------------------------------*/


/* Private variables ---------------------------------------------------------*/
 PORT_InitTypeDef PORT_InitStructure;
 
 #define pause 2000
 #define fcpu 79999 //������� ��

//_____________________________________________________________________________________________________

 void CLK_init(void);
 void SYSTICK_init(void);
 void GPIO_init(void);



 long sys_time_count = pause;

 //__________________________________________________________
 
 int main()
 {
	 //��������� ��
   CLK_init();
	 SYSTICK_init();
	 GPIO_init();
	 
	 //
   while (1)
   {
		 
		 if(sys_time_count == 0)
		 {
			 sys_time_count = pause;
		 }
   }
 }


 //������� ��������� ������������
 void CLK_init(void)
 {
	 RST_CLK_DeInit(); //����� �������� ������������
	 RST_CLK_HSEconfig(RST_CLK_HSE_ON); //��������� ������������ �� �������� ����������
	 //���� ���������� ������ �������� ����������
	 
	 while(RST_CLK_HSEstatus() != SUCCESS)
	 {
		 __NOP();
	 }
	 
	 //�������� ������ ������� �������������� �������
	 RST_CLK_CPU_PLLcmd(ENABLE);
	 //�������� ������� �������� ������������ 
	 //� ��������� ��������� ��� �������
	 //CPU_C1_SEL
	 RST_CLK_CPU_PLLconfig(RST_CLK_CPU_PLLsrcHSEdiv1, RST_CLK_CPU_PLLmul10); //80 ���
	 //���� ���������� ������
	 while(RST_CLK_CPU_PLLstatus() != SUCCESS)
	 {
		 __NOP();
	 }
	 //������� �������� ������������ � ���������� (��� ���)
	 //CPU_C2_SEL
   //RST_CLK_CPU_PLLuse(DISABLE);
	 RST_CLK_CPU_PLLuse(ENABLE);
	 
	 //�������� ������������ �������� ������� (���� ����������)
	 //CPU_C3_SEL
   RST_CLK_CPUclkPrescaler(RST_CLK_CPUclkDIV1); 
	 //RST_CLK_CPUclkSelection(RST_CLK_CPUclkCPU_C3);
	 // ��������� ���������� ���������� ���������� SelectRI � LOW � ����������� BKP
   // �������� ������� 80 ���
   RST_CLK_PCLKcmd(RST_CLK_PCLK_BKP, ENABLE);
	 //MDR_BKP -> REG_0E |= DUcc_Mask & POWER_DUcc_upto_80MHz;
   POWER_DUccMode(POWER_DUcc_upto_80MHz);
   //������� �������� ������������
	 //HCLK_SEL
   RST_CLK_CPUclkSelection(RST_CLK_CPUclkCPU_C3);
	 //RST_CLK_HSIcmd(DISABLE); //��������� ���������� ���������
	 //��������� ������������
	 SystemCoreClockUpdate();
 } 
  
 //��������� ���������� �������
 void SYSTICK_init(void)
 {
	 SysTick->LOAD = fcpu;
	 SysTick->CTRL |= SysTick_CTRL_CLKSOURCE_Msk;
	 SysTick->CTRL |= SysTick_CTRL_TICKINT_Msk;
	 SysTick->CTRL |= SysTick_CTRL_ENABLE_Msk;
	 NVIC_EnableIRQ(SysTick_IRQn);
 }
 
 //��������� GPIO
 void GPIO_init(void)
 {
	 PORT_InitTypeDef GPIOInitStruct;
   RST_CLK_PCLKcmd (RST_CLK_PCLK_PORTC, ENABLE); //��������� ��������� ����� C
   PORT_StructInit(&GPIOInitStruct);
   GPIOInitStruct.PORT_Pin = PORT_Pin_0; //VD3
   GPIOInitStruct.PORT_OE = PORT_OE_OUT;
   GPIOInitStruct.PORT_SPEED = PORT_SPEED_FAST;
   GPIOInitStruct.PORT_MODE = PORT_MODE_DIGITAL;
   PORT_Init(MDR_PORTC, &GPIOInitStruct);
	 
	 PORT_StructInit(&GPIOInitStruct);
	 GPIOInitStruct.PORT_Pin = PORT_Pin_1; //VD4
   GPIOInitStruct.PORT_OE = PORT_OE_OUT;
   GPIOInitStruct.PORT_SPEED = PORT_SPEED_FAST;
   GPIOInitStruct.PORT_MODE = PORT_MODE_DIGITAL;
   PORT_Init(MDR_PORTC, &GPIOInitStruct);
	 
	 PORT_StructInit(&GPIOInitStruct);
	 GPIOInitStruct.PORT_Pin = PORT_Pin_2; //SELECT
   GPIOInitStruct.PORT_OE = PORT_OE_IN;
   GPIOInitStruct.PORT_SPEED = PORT_SPEED_FAST;
   GPIOInitStruct.PORT_MODE = PORT_MODE_DIGITAL;
   PORT_Init(MDR_PORTC, &GPIOInitStruct);
	 
	 RST_CLK_PCLKcmd (RST_CLK_PCLK_PORTE, ENABLE); //��������� ��������� ����� E 
	 PORT_StructInit(&GPIOInitStruct);
	 GPIOInitStruct.PORT_Pin = PORT_Pin_1; //DOWN
   GPIOInitStruct.PORT_OE = PORT_OE_IN;
   GPIOInitStruct.PORT_SPEED = PORT_SPEED_FAST;
   GPIOInitStruct.PORT_MODE = PORT_MODE_DIGITAL;
   PORT_Init(MDR_PORTE, &GPIOInitStruct);
	 
	 PORT_StructInit(&GPIOInitStruct);
	 GPIOInitStruct.PORT_Pin = PORT_Pin_3; //LEFT
   GPIOInitStruct.PORT_OE = PORT_OE_IN;
   GPIOInitStruct.PORT_SPEED = PORT_SPEED_FAST;
   GPIOInitStruct.PORT_MODE = PORT_MODE_DIGITAL;
   PORT_Init(MDR_PORTE, &GPIOInitStruct);
	 
	 RST_CLK_PCLKcmd (RST_CLK_PCLK_PORTB, ENABLE); //��������� ��������� ����� B
	 PORT_StructInit(&GPIOInitStruct);
	 GPIOInitStruct.PORT_Pin = PORT_Pin_6; //RIGHT
   GPIOInitStruct.PORT_OE = PORT_OE_IN;
   GPIOInitStruct.PORT_SPEED = PORT_SPEED_FAST;
   GPIOInitStruct.PORT_MODE = PORT_MODE_DIGITAL;
   PORT_Init(MDR_PORTB, &GPIOInitStruct);
	 
	 PORT_StructInit(&GPIOInitStruct);
	 GPIOInitStruct.PORT_Pin = PORT_Pin_5; //UP
   GPIOInitStruct.PORT_OE = PORT_OE_IN;
   GPIOInitStruct.PORT_SPEED = PORT_SPEED_FAST;
   GPIOInitStruct.PORT_MODE = PORT_MODE_DIGITAL;
   PORT_Init(MDR_PORTB, &GPIOInitStruct);
 }
 


 //-------------------------------����������----------------------------- 
 
 //��������� ������
 void SysTick_Handler()
 {
	 sys_time_count--;
 }
